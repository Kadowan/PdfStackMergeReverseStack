# PdfStackMergeReverseStack

The intention of this project is to merge PDFs of a scanner, that can only scan one sided Pages.  

# Visualised Process
Actual pagenumber are represented by the number in the Array
>>>
1. original Document [1, 2, 3, 4, 5, 6]
2. scanning Frontpages [1, 3, 5]
3. scanning Backpages in reversed order [6, 4, 2]
4. reverse Backpage order [2, 4, 6]
5. merging the two Arrays [1, 2, 3, 4, 5, 6]
>>>

# Guide
1. Scan the frontside of the Stack
2. Flip the whole stack arround to scan the backside. The last page should be scanned first.
3. Execute the script and a new file named merged_"datetime" appears in the directory
