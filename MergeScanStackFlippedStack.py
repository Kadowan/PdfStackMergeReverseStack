#!/bin/python3
from PyPDF2 import PdfFileWriter, PdfFileReader
from os import listdir
import getpass
import datetime

scan_file_path = '/run/media/' + getpass.getuser() + '/NAS/HPSCANS'
dir_list = sorted(listdir(scan_file_path))
most_recent_file = dir_list[-1]
second_recent_file = dir_list[-2]

output_writer = PdfFileWriter()


pdf_stack_front = PdfFileReader(open(scan_file_path + "/" + second_recent_file, 'rb'))
pdf_reversestack_back = PdfFileReader(open(scan_file_path + "/" + most_recent_file, 'rb'))

pdf_stack_back = PdfFileWriter()
total_pages = pdf_reversestack_back.getNumPages()
for page in range(total_pages - 1, -1, -1):
    pdf_stack_back.addPage(pdf_reversestack_back.getPage(page))

max_length = 0
len_front = pdf_stack_front.getNumPages()
len_back = pdf_stack_back.getNumPages()
if len_front >= len_back:
	max_length = len_front
else:
	max_length = len_back 

for i in range(max_length) :
	try: 
		output_writer.addPage(pdf_stack_front.getPage(i))
		output_writer.addPage(pdf_stack_back.getPage(i))
	except IndexOutOfBound:
	 ...

output_file=open(scan_file_path + "/" + "merged_" + datetime.datetime.now().strftime("%Y%m%d%H%M%S") + ".pdf","wb")
output_writer.write(output_file)
output_file.close()
